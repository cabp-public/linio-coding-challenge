<?php

use App\AppNumbers;

require '../vendor/autoload.php';

$min = 1;
$max = 100;

$options = [
    'iterables' => [
        [
            'value' => 3,
            'output' => 'Linio'
        ],
        [
            'value' => 5,
            'output' => 'IT'
        ]
    ],
    'group' => [
        'value' => [3,5],
        'output' => 'Linianos',
    ],
];

$mainApp = new AppNumbers();
$mainApp->run($min, $max, $options);
$mainApp->printResults();
