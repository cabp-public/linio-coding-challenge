# Test Linio

Test repository for Linio

#### How to Install
Clone this repo then run composer install

#### Running The App
Go to the public folder and run `php index.php`

#### Default Data
Within the public folder you'll find the `index.php` file, it contains default data for the challenge. You can set a numeric range (i.e. min and max values) as well as a group to match more than one multiple, as follows:

    $options = [
        'iterables' => [
            [
                'value' => 3,
                'output' => 'Linio'
            ],
            [
                'value' => 5,
                'output' => 'IT'
            ]
        ],
        'group' => [
            'value' => [3,5],
            'output' => 'Linianos',
        ],
    ];

#### Running Test Suites
The `behat.yml` file includes settings for the `default` suite, here you can include more suites according to your needs. To run tests simply go to the project folder and run:

    $ vendor/bin/behat
