@numbers
Feature: NUMBERS
  In order to replace multiples of 3 and 5
  As a User
  I need to be able to set the range and options

#  Scenario: Register new user
#    Given User e-mail is "john.doe@hotmail.com"
#    And User password is "1234"
#    When User tries to register
#    Then Consumer gets a 200 response

#  Scenario: Register an existent user
#    Given User e-mail is "uncle_ben@hotmail.com"
#    And User password is "1234"
#    When User tries to register
#    Then Consumer gets a 409 response

  Scenario: Requested Behaviour
    Given Minimum value is 1
    And Maximum value is 15
    And Iterables are:
      | value | output |
      | 3     | Linio  |
      | 5     | IT     |
    And Group values are:
      | 3 | 5 |
    And Group output is "Linianos"
    When User runs the app
    Then Result for number 1 should be "1"
    And Result for number 2 should be "2"
    And Result for number 3 should be "Linio"
    And Result for number 5 should be "IT"
    And Result for number 15 should be "Linianos"
