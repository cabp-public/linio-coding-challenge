<?php

use Behat\Behat\Context\Context;

abstract class TestContext extends \PHPUnit\Framework\TestCase implements Context
{
    protected $app;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }

    /**
     * Creates the application.
     */
    public function createApplication()
    {
        $this->app = new \App\AppNumbers();
    }
}