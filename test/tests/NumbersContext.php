<?php

/**
 * Defines application features from the API context.
 */
class NumbersContext extends TestContext
{
    private $max;
    private $min;
    private $iterables;
    private $groupValues;
    private $groupOutput;
    private $results;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @Given /^Maximum value is (\d+)$/
     * @param int $max
     */
    public function setRangeMax(int $max)
    {
        try {
            $this->max = $max;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Minimum value is (\d+)$/
     * @param int $min
     */
    public function setRangeMin(int $min)
    {
        try {
            $this->min = $min;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Iterables are:$/
     * @param \Behat\Gherkin\Node\TableNode $iterables
     */
    public function setIterables(\Behat\Gherkin\Node\TableNode $iterables)
    {
        try {
            $this->iterables = $iterables;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Group values are:$/
     * @param \Behat\Gherkin\Node\TableNode $groupValues
     */
    public function setGroupValues(\Behat\Gherkin\Node\TableNode $groupValues)
    {
        try {
            $values = $groupValues->getRows()[0];

            foreach ($values as $value) {
                $this->groupValues[] = $value;
            }
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Group output is "([^"]*)"$/
     * @param string $value
     */
    public function setGroupOutput(string $value)
    {
        try {
            $this->groupOutput = $value;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @When /^User runs the app$/
     */
    public function userRunsTheApp()
    {
        try {
            $options = [
                'iterables' => $this->iterables,
                'group' => [
                    'value' => $this->groupValues,
                    'output' => $this->groupOutput,
                ]
            ];

            $this->app->run($this->min, $this->max, $options);
            $this->results = $this->app->getResults();
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Then /^Result for number (\d+) should be "([^"]*)"$/
     * @param int $number
     * @param string $expectedOutput
     */
    public function resultForNumberShouldBe(int $number, string $expectedOutput)
    {
        try {
            $position = $number - 1;
            $output = $this->results[$position];

            $this->assertEquals($expectedOutput, $output);
        }
        catch (\Exception $exception) {}
    }
}
