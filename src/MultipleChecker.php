<?php

namespace App;

final class MultipleChecker
{
    private $iterables;
    private $group;

    function __construct(array $options)
    {
        $this->iterables = $options['iterables'];
        $this->group = $options['group'];
    }

    private function checkGroup($matchHistory, $defaultOutput)
    {
        $output = $defaultOutput;

        try {
            $historyValues = array_filter($matchHistory);
            array_multisort($historyValues, SORT_ASC);

            $groupValues = $this->group['value'];
            array_multisort($groupValues, SORT_ASC);

            if ($historyValues === $groupValues) {
                $output = $this->group['output'];
            }
        }
        catch (\Exception $exception) {
            $output = $defaultOutput;
        }
        finally {
            return $output;
        }
    }

    private function matchOutput($number)
    {
        $matches = [$number];
        $matchHistory = [];

        foreach ($this->iterables as $index => $entry) {
            $value = $entry['value'];
            $rest = $number % $value;
            $matchHistory[$index] = '';

            if ($rest === 0) {
                $matches[0] = $entry['output'];
                $matchHistory[$index] = $value;
            }
        }

        $cleanMatch = array_filter($matches);
        $defaultOutput = implode('', $cleanMatch);
        $output = $this->checkGroup($matchHistory, $defaultOutput);

        return $output;
    }

    public function getValue($number)
    {
        return $this->matchOutput($number);
    }
}
