<?php

namespace App;

final class AppNumbers
{
    private $results;

    function __construct()
    {
        $this->results = [];
    }

    private function printLn($message)
    {
        echo strval($message) . "\n";
    }

    public function run($min, $max, $options)
    {
        try {
            $checker = new MultipleChecker($options);

            for ($n = $min; $n <= $max; $n++) {
                $output = $checker->getValue($n);
                $this->results[] = $output;
            }
        }
        catch (\Exception $exception) {
            $this->results[] = $exception->getMessage();
        }
    }

    public function getResults()
    {
        return $this->results;
    }

    public function printResults()
    {
        foreach ($this->results as $result) {
            $this->printLn($result);
        }
    }
}
